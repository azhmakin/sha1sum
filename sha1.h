
#pragma once

#include <stdint.h>

typedef uint32_t SHA1_HASH[5]; // 160 bits, 20 bytes
typedef uint32_t SHA1_CHUNK[16]; // 512 bits, 64 bytes

void sha1_init(SHA1_HASH hash);
void sha1_process_chunk(SHA1_HASH hash, SHA1_CHUNK chunk);

void sha1_finalize(SHA1_HASH hash, SHA1_CHUNK chunk, uint64_t message_length);

void sha1_hash_string(SHA1_HASH hash, const char *str);

void sha1_hash_to_string(SHA1_HASH hash, char *str);
