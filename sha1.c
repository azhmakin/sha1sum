
#include <stdint.h>
#include <assert.h>

#include "sha1.h"


void sha1_init(SHA1_HASH hash)
{
    hash[0] = UINT32_C(0x67452301);
    hash[1] = UINT32_C(0xEFCDAB89);
    hash[2] = UINT32_C(0x98BADCFE);
    hash[3] = UINT32_C(0x10325476);
    hash[4] = UINT32_C(0xC3D2E1F0);
}


#define LEFT_ROTATE(x,s) ( ( (x) << (s) ) | ( (x) >> (32 - (s)) ) )


void sha1_process_chunk(SHA1_HASH hash, SHA1_CHUNK chunk)
{
    uint32_t w[80];

    for (int i = 0; i < 16; i++)
    {
        w[i] = chunk[i];
    }

    for (int i = 16; i < 80; i++)
    {
        uint32_t temp = w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16];
        w[i] = LEFT_ROTATE(temp, 1);
    }

    uint32_t a = hash[0];
    uint32_t b = hash[1];
    uint32_t c = hash[2];
    uint32_t d = hash[3];
    uint32_t e = hash[4];

    for (int i = 0; i < 80; i++)
    {
        uint32_t k, f, temp;

        if (i < 20)
        {
            f = (b & c) | ((~b) & d);
            k = UINT32_C(0x5A827999);
        }
        else if (i < 40)
        {
            f = b ^ c ^ d;
            k = UINT32_C(0x6ED9EBA1);
        }
        else if (i < 60)
        {
            f = (b & c) | (b & d) | (c & d);
            k = UINT32_C(0x8F1BBCDC);
        }
        else
        {
            f = b ^ c ^ d;
            k = UINT32_C(0xCA62C1D6);
        }

        temp = LEFT_ROTATE(a, 5) + f + e + k + w[i];
        e = d;
        d = c;
        c = LEFT_ROTATE(b, 30);
        b = a;
        a = temp;
    }

    hash[0] += a;
    hash[1] += b;
    hash[2] += c;
    hash[3] += d;
    hash[4] += e;
}


void set_bit(SHA1_CHUNK chunk, int index, int value)
{
    int iW = index / 32;
    int iB = 31 - index % 32;

    if (value != 0)
    {
        chunk[iW] |= UINT32_C(1) << iB;
    }
    else
    {
        chunk[iW] &= ~(UINT32_C(1) << iB);
    }
}


void sha1_finalize(SHA1_HASH hash, SHA1_CHUNK chunk, uint64_t message_length)
{
    int finalBits = message_length % 512;

    int emptyBits = 512 - finalBits;

    // We have enough space in this chunk to complete computation
    if (emptyBits >= 65)
    {
        // Append '1'
        set_bit(chunk, finalBits, 1);

        // Fill with zeros
        for (int i = finalBits + 1; i < 448; i++)
        {
            set_bit(chunk, i, 0);
        }

        chunk[14] = (message_length >> 32) & 0xFFFFFFFF;
        chunk[15] = (message_length      ) & 0xFFFFFFFF;

        sha1_process_chunk(hash, chunk);
    }
    else
    {
        if (emptyBits > 0)
        {
            // Append '1'
            set_bit(chunk, finalBits, 1);

            // Fill with zeros
            for (int i = finalBits + 1; i < 512; i++)
            {
                set_bit(chunk, i, 0);
            }

            sha1_process_chunk(hash, chunk);
        }

        SHA1_CHUNK temp = {0};

        if (emptyBits == 0)
        {
            set_bit(temp, 0, 1);
        }

        temp[14] = (message_length >> 32) & 0xFFFFFFFF;
        temp[15] = (message_length      ) & 0xFFFFFFFF;

        sha1_process_chunk(hash, temp);
    }
}


// TODO: Describe the order of writing!!!
void set_byte_in_chunk(SHA1_CHUNK chunk, uint8_t value, int index)
{
    assert(index >= 0 && index < 64);

    int iW = index / 4;
    int iB = index % 4;

    if (iB == 0)
    {
        chunk[iW] = (uint32_t) value << 24;
    }
    else
    {
        chunk[iW] |= (uint32_t) value << ((3 - iB) * 8);
    }
}


void sha1_hash_string(SHA1_HASH hash, const char *str)
{
    SHA1_CHUNK chunk;

    sha1_init(hash);

    uint64_t index = 0;

    while (str[index] != '\0')
    {
        char c = str[index];

        set_byte_in_chunk(chunk, c, index % 64);

        index++;

        if (index % 64 == 0)
        {
            sha1_process_chunk(hash, chunk);
        }
    }

    sha1_finalize(hash, chunk, index * 8);
}


const static char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};


void sha1_hash_to_string(SHA1_HASH hash, char *str)
{
    for (int i = 0; i < 5; i++)
    {
        for (int j = 32; (j -= 4) >= 0; )
        {
            *str++ = digit[ (hash[i] >> j) & 0x0F ];
        }
    }

    *str = '\0';
}

