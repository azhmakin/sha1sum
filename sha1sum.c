
#include <stdio.h>

#include "sha1.h"


void print_hash(SHA1_HASH hash)
{
    char str[41];

    sha1_hash_to_string(hash, str);

    printf("%s\n", str);

    /*
    const static char digit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    for (int i = 0; i < 5; i++)
    {
        for (int j = 32; (j -= 4) >= 0; )
        {
            printf("%c", digit[ (hash[i] >> j) & 0x0F ]);
        }
    }

    printf("\n");*/
}


void test(const char *s)
{
    SHA1_HASH hash;

    sha1_hash_string(hash, s);
    print_hash(hash);
}


int main()
{
    test("");
    test("a");
    test("abc");

    /*SHA1_HASH hash;
    SHA1_CHUNK chunk = { 0 };

    sha1_init(hash);
    chunk[0] = UINT32_C(0x80000000);

    sha1_process_chunk(hash, chunk);

    print_hash(hash);*/
}
